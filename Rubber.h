﻿
// Rubber.h: PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含 'pch.h' 以生成 PCH"
#endif

#include "resource.h"		// 主符号
#include "Splash.h"


// CRubberApp:
// 有关此类的实现，请参阅 Rubber.cpp
//

class CRubberApp : public CWinApp
{
public:
	CRubberApp();
	CSplashWnd SplashWindow;

// 重写
public:
	virtual BOOL InitInstance();

// 实现
	CString globalAcadLocation;
	bool GetSpecifiedAutoCADPath(CString& strAcadLocation);
	bool LoadArx(CString ArxFname, CString cmd);
	CString GetCurrentDirectory(HMODULE hInstance);
	DECLARE_MESSAGE_MAP()
};

extern CRubberApp theApp;
