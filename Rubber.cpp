﻿
// Rubber.cpp: 定义应用程序的类行为。
//

#include "pch.h"
#include "framework.h"
#include "Rubber.h"
#include "RubberDlg.h"
#include "Registry.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


STARTUPINFO sb;
PROCESS_INFORMATION pprocess;
CString systemPath;
CString localAutoCadPath;

// CRubberApp

BEGIN_MESSAGE_MAP(CRubberApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CRubberApp 构造

CRubberApp::CRubberApp()
{
	// 支持重新启动管理器
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的 CRubberApp 对象

CRubberApp theApp;


// CRubberApp 初始化

BOOL CRubberApp::InitInstance()
{
	systemPath = GetCurrentDirectory(NULL); // 软件系统路径
	if (GetSpecifiedAutoCADPath(globalAcadLocation) == false)
	{
		AfxMessageBox(L"请以管理员身份运行本程序(操作：鼠标左键选中执行程序，右键选择管理员身份运行)!");
		return FALSE;
	}
	localAutoCadPath.Format(L"%s\\acad.exe", globalAcadLocation);
	SplashWindow.ShowSplashScreen();
	Sleep(2000);
	LoadArx(L"Pencil.arx", L"");
	WinExec("", SW_SHOW);
	return FALSE;
}


bool CRubberApp::GetSpecifiedAutoCADPath(CString& strAcadLocation)
{
	CRegistry reg;
	BOOL bReturn = FALSE;
	//bReturn = reg.Open (L"SOFTWARE\\Autodesk\\AutoCAD\\R16.2\\ACAD-4001:804\\"); // CAD 2006 中文版
	bReturn = reg.Open(L"SOFTWARE\\Autodesk\\AutoCAD\\R19.1\\ACAD-D001:804"); // CAD 2014 // 可在CAD注册表查看
	//bReturn = reg.Open(L"SOFTWARE\\Autodesk\\AutoCAD\\R20.1\\ACAD-F001:804"); // CAD 2016
	//bReturn = reg.Open(L"SOFTWARE\\Autodesk\\AutoCAD\\R24.0\\ACAD-4101:804"); // CAD 2021
	//bReturn = reg.Open (L"SOFTWARE\\Autodesk\\AutoCAD\\R18.0\\ACAD-8001:409"); // CAD 2010 英文版
	bReturn = reg.Read(L"AcadLocation", &strAcadLocation);
	return bReturn != 0;
}

bool CRubberApp::LoadArx(CString ArxFname, CString cmd)
{
	FILE* pfw = NULL;
	CString TMPPATH;
	TMPPATH.Format(L"%s\\command.scr", systemPath);
	// 重写调用CAD后执行的命令文件

	_wsetlocale(0, L"chs");//务必加上否则生成的文本是问号字符串
	_wfopen_s(&pfw, TMPPATH, L"w");
	if (pfw == NULL)
		return false;
	fwprintf(pfw, L"arx\n");
	fwprintf(pfw, L"load\n");
	fwprintf_s(pfw, L"\"%s\\%s\"\n", systemPath, ArxFname);
	if (cmd != L"")
		fwprintf(pfw,L"%s ", cmd);

	fwprintf(pfw, L"ribbon\n");
	fwprintf(pfw, L"netload\n");
	fwprintf(pfw, L"\"%ws\\%ws\"\n", systemPath, L"library\\MyRibbonMenu.dll"); // 库文件
	fwprintf(pfw, L"MyMenu\n"); // 命令
	fclose(pfw);
	CString NewTmpLocation = L"";
	NewTmpLocation.Format(L"%s\\command.scr", globalAcadLocation);
	DeleteFile(NewTmpLocation);
	CopyFile(TMPPATH, NewTmpLocation, TRUE);

	sb.cb = sizeof(sb);
	sb.wShowWindow = SW_SHOWMAXIMIZED;
	sb.dwFlags = STARTF_USESHOWWINDOW;
	if (CreateProcess(localAutoCadPath, L" /b command", NULL, NULL, FALSE, 0, NULL, NULL, &sb, &pprocess))
		return true;
	else
		return false;
}

CString CRubberApp::GetCurrentDirectory(HMODULE hInstance)
{
	TCHAR szPath[256];
	GetModuleFileName(hInstance, szPath, sizeof(szPath));
	*(_tcsrchr(szPath, '\\')) = 0;		// 将最后一个\所在的位置修改为\0

	CString strResult = szPath;
	return strResult;
}
